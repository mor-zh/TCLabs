﻿using CodingAlgorithms.ShannonFano;
using CodingAlgorithms.Huffman;
using CodingAlgorithms.Arithmetic;
using CodingAlgorithms.BWT_RLE;
using CodingAlgorithms.LZ77;

string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
string encoded;
string decoded;

ShannonFanoEncoder sfe = new ShannonFanoEncoder();

encoded = sfe.Encode(text);
decoded = sfe.Decode(encoded);

System.Console.WriteLine("\tShannon-Fano");
System.Console.WriteLine(encoded);
System.Console.WriteLine(decoded);

HuffmanEncoder he = new HuffmanEncoder();

encoded = he.Encode(text);
decoded = he.Decode(encoded);

System.Console.WriteLine("\tHuffman");
System.Console.WriteLine(encoded);
System.Console.WriteLine(decoded);

ArithmeticEncoder ae = new ArithmeticEncoder();

encoded = ae.Encode(text);
decoded = ae.Decode(encoded);

System.Console.WriteLine("\tArithmetic");
System.Console.WriteLine(encoded);
System.Console.WriteLine(decoded);

RLEBWTCompressor rbc = new RLEBWTCompressor();

encoded = rbc.Compress(text);
decoded = rbc.Decompress(encoded);

System.Console.WriteLine("\tRLE_BWT");
System.Console.WriteLine(encoded);
System.Console.WriteLine(decoded);

LZ77Encoder lz = new LZ77Encoder();

encoded = lz.Encode(text);
decoded = lz.Decode(encoded);

System.Console.WriteLine("\tLZ77");
System.Console.WriteLine(encoded);
System.Console.WriteLine(decoded);