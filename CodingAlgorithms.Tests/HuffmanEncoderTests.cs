using Xunit;
using CodingAlgorithms.Huffman;

namespace CodingAlgorithms.Tests;

public class HuffmanEncoderTests
{
    [Fact]
    public void FrequenciesAreOrdered()
    {
        string text = "beep boop beer!";

        HuffmanEncoder he = new HuffmanEncoder();

        var freq = he.GetCharsQuantites(text);
        var lf = he.SortCharQuantities(freq);

        System.Console.WriteLine(lf.ToString());
    }
}