
using System.Text;

namespace CodingAlgorithms.Hamming
{
    public class HammingEncoder {
        public string Encode(string text) {
            var cbits = CountControlBits(text);
            var arrbits = InsertControlBits(text, cbits);
            var calculated = CalculateControlBits(arrbits, cbits);

            return calculated;
        }
        public string Decode(string text) {
            var errorBitPos = CheckControlBits(text);
            var sb = new StringBuilder(text);
            var errorBit = sb[errorBitPos - 1] - '0';
            var corrbit = (char)((errorBit + 1) % 2 + '0');
            sb[errorBitPos - 1] = corrbit;
            return sb.ToString();
        }

        public int CountControlBits(string text) {
            int cnt = text.Length;
            int powTwo = 1;
            while (powTwo <= cnt)
            {
                powTwo *= 2;
                cnt++;
            }

            return cnt - text.Length;
        }

        public string InsertControlBits(string text, int bitsCount) {
            StringBuilder sb = new StringBuilder(text);

            int cbitPos = 1;
            for (int i = 0; i < bitsCount; i++)
            {
                sb.Insert(cbitPos - 1, "0");
                cbitPos *= 2;
            }

            return sb.ToString();
        }

        public string CalculateControlBits(string text, int bitsCount) {
            int cbitPow = 1;
            StringBuilder sb = new StringBuilder(text);

            for (int i = 0; i < bitsCount; i++)
            {
                int cnt = 0;
                bool counting = true;
                int cntCounting = cbitPow;

                sb[cbitPow - 1] = '0';
                for (int j = cbitPow - 1; j < sb.Length; j++)
                {
                    if (counting) {
                        cnt += int.Parse(sb[j].ToString());
                        cntCounting--;

                        if (cntCounting == 0) counting = false;
                    }
                    else {
                        cntCounting++;

                        if (cntCounting == cbitPow) counting = true;
                    }
                }

                sb[cbitPow - 1] = (char)((cnt % 2) + '0');

                cbitPow *= 2;
            }

            return sb.ToString();
        }

        public int CheckControlBits(string text) {
            int pow = 1;
            int cnt = 1;
            while (pow < text.Length) {
                pow *= 2;
                cnt++;
            }

            string comparisonString = CalculateControlBits(text, cnt - 1);

            List<int> wrongMatches = new List<int>(cnt - 1);           
            for (int i = 1; i < text.Length; i *= 2)
            {
                if (text[i - 1] != comparisonString[i - 1]) wrongMatches.Add(i);
            }

            return wrongMatches.Sum();
        }
    }
}