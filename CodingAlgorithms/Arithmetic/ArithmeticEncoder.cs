﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace CodingAlgorithms.Arithmetic
{
    public class ArithmeticEncoder
    {
        private Dictionary<char, long> _frequency;
        private IDictionary<char, long> _cumulativeFreq;
        int _power = 0;
        BigInteger _number = 0;

        public string Encode(string str)
        {
            BigInteger baseValue = str.Length,
                lowerValue = 0,
                productFreq = 1;

            SetAlphabet(str);
            foreach (var c in str)
            {
                BigInteger currentCumFreq = _cumulativeFreq[c];
                lowerValue = lowerValue * baseValue + currentCumFreq * productFreq;
                productFreq *= _frequency[c];
            }

            BigInteger upper = lowerValue + productFreq,
                bigRadix = 10;
            _power = 0;

            while (true)
            {
                productFreq /= bigRadix;
                if (productFreq == 0) break;
                _power++;
            }

            var diff = (upper - 1) / (BigInteger.Pow(bigRadix, _power));
            _number = diff;
            return $"{diff} * 10^{_power}";
        }

        public string Decode(string source)
        {
            SetUpCumulativeFrequency();
            BigInteger powr = 10,
            enc = _number * BigInteger.Pow(powr, _power);
            var sumOfFrequencies = _frequency.Values.Sum();

            var reverseCumFreq = new Dictionary<long, char>();
            foreach (var key in _cumulativeFreq.Keys)
            {
                var value = _cumulativeFreq[key];
                reverseCumFreq[value] = key;
            }

            var freq = -1L;
            for (long i = 0; i < sumOfFrequencies; i++)
            {
                if (reverseCumFreq.ContainsKey(i))
                {
                    freq = reverseCumFreq[i];
                }
                else if (freq != -1)
                {
                    reverseCumFreq[i] = (char)freq;
                }
            }

            var decoded = new StringBuilder((int)sumOfFrequencies);
            BigInteger bigBase = sumOfFrequencies;
            for (var i = sumOfFrequencies - 1; i >= 0; --i)
            {
                BigInteger pow = BigInteger.Pow(bigBase, (int)i),
                    div = enc / pow;
                var c = reverseCumFreq[(long)div];
                BigInteger fv = _frequency[c],
                    cv = _cumulativeFreq[c],
                    diff = enc - pow * cv;
                enc = diff / fv;
                decoded.Append(c);
            }

            return decoded.ToString();
        }
        private void FillFrequencyList(string str, bool needsSort)
        {
            _frequency = new Dictionary<char, long>();

            foreach (var c in str)
            {
                if (!_frequency.ContainsKey(c))
                    _frequency[c] = 0;

                _frequency[c]++;
            }

            SetUpCumulativeFrequency();
        }

        //private void SetupDecoder(IReadOnlyList<object> args)
        //{
        //    _frequency = args[0] as IDictionary<char, long>;
        //    _number = (BigInteger)args[1];
        //    _power = (int)args[2];

        //    SetUpCumulativeFrequency();
        //}

        private void SetUpCumulativeFrequency()
        {
            _cumulativeFreq = new Dictionary<char, long>();

            var total = 0L;
            for (var i = 0; i < 2048; i++)
            {
                var c = (char)i;
                if (_frequency.ContainsKey(c))
                {
                    var currentFreq = _frequency[c];
                    _cumulativeFreq[c] = total;
                    total += currentFreq;
                }
            }
        }

        public void SetAlphabet(string source)
        {
            FillFrequencyList(source, false);
        }

        public IEnumerable<string> GetAlphabetToString()
        {
            foreach (var v in _frequency) { yield return v.Key+" "+v.Value; }
        }
    }
}