using System.Text;

namespace CodingAlgorithms.BWT_RLE
{
    public class BWTTransformer {
        public string Transform(string text) {
            //get all rotations
            var rotations = GetAllRotations(text);

            //sort all rotations
            var sortedRots = SortRotations(rotations);

            //get transformed string
            var transform = GetTransform(sortedRots);
            return transform;
        }

        public string BackwardTransform(string transformedText) {
            //add column

            //sort column

            //add column

            //sort column

            //...
            var brots = GetAllBackwardRotations(transformedText);

            //get by pos
            return GetOriginalString(brots);
        }

        public List<string> GetAllRotations(string text) {
            text += "$";
            int tLength = text.Length;
            List<string> rotations = new List<string>(tLength);
            rotations.Add(text);

            for (int i = 1; i < tLength; i++)
            {
                rotations.Add(ShiftString(rotations[i - 1]));
            }

            return rotations;
        }
        private string ShiftString(string text) {
            return text.Substring(1, text.Length - 1) + text.Substring(0, 1); 
        }

        public List<string> SortRotations(List<string> rotations) {
            rotations.Sort();

            return rotations;
        }

        public string GetTransform(List<string> sortedRotations) {
            int lastSymPos = sortedRotations[0].Length - 1;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < sortedRotations.Count; i++)
            {
                sb.Append(sortedRotations[i][lastSymPos]);
            }

            return sb.ToString();
        }

        public string[] MakeColumn(string transformedText) {
            var rots = new string[transformedText.Length];

            for (int i = 0; i < transformedText.Length; i++)
            {
                rots[i] = transformedText[i].ToString();
            }

            return rots;
        }

        public string[] AppendColumn(string[] rotations, string text) {
            for (int i = 0; i < rotations.Length; i++)
            {
                rotations[i] = text[i] + rotations[i];
            }

            return rotations;
        }

        public string[] GetAllBackwardRotations(string text) {
            var rots = MakeColumn(text);
            Array.Sort(rots);

            for (int i = 1; i < rots.Length; i++)
            {
                rots = AppendColumn(rots, text);
                Array.Sort(rots);
            }

            return rots;
        }

        public string GetOriginalString(string[] rotations) {
            int lastSym = rotations[0].Length - 1;

            for (int i = 0; i < rotations.Length; i++)
            {
                if (rotations[i][lastSym] == '$') return rotations[i].Substring(0, lastSym);
            }

            return "";
        }
    }
}