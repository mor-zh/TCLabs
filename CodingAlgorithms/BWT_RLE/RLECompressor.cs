using System.Text;

namespace CodingAlgorithms.BWT_RLE
{
    public class RLECompressor {
        public string Compress(string text) {
            StringBuilder res = new StringBuilder();

            int cnt = 1;
            bool countingRepeats = false;
            StringBuilder sb = new StringBuilder();
            sb.Append(text[0]);

            for (int i = 1; i < text.Length; i++)
            {
                if (text[i - 1] != text[i]) {
                    if (!countingRepeats) {
                        cnt++;
                        sb.Append(text[i]);
                    }
                    else {
                        res.Append($"{cnt}{sb.ToString()}");

                        countingRepeats = false;
                        cnt = 1;
                        sb.Clear();
                        sb.Append(text[i]);
                    }
                }
                else {
                    if (countingRepeats) {
                        cnt++;
                    }
                    else {
                        cnt--;
                        if (cnt != 0) {
                            res.Append($"-{cnt}{sb.ToString().Substring(0, cnt)}");
                        }

                        countingRepeats = true;
                        cnt = 2;
                        sb.Clear();
                        sb.Append(text[i]);
                    }
                }
            }

            if (countingRepeats) {
                res.Append($"{cnt}{sb.ToString()}");
            }
            else {
                res.Append($"-{cnt}{sb.ToString()}");
            }

            return res.ToString();
        }
        public string Decompress(string text) {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '-') {
                    int j = i + 1;
                    while(char.IsDigit(text[j])) {
                        j++;
                    }

                    j--;

                    int cnt = int.Parse(text.Substring(i + 1, j - i));

                    sb.Append(text.Substring(j + 1, cnt));

                    i = j + cnt;
                }
                else if (char.IsDigit(text[i])) {
                    int h = i + 1;
                    while (char.IsDigit(text[h])) {
                        h++;
                    }

                    int cnt = int.Parse(text.Substring(i, h - i));                    
                    for (int j = 0; j < cnt; j++)
                    {
                        sb.Append(text[h]);
                    }

                    i = h;
                }
            }

            return sb.ToString();
        }
    }
}