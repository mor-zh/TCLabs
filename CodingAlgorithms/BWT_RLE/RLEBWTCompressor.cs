namespace CodingAlgorithms.BWT_RLE
{
    public class RLEBWTCompressor {
        public string Compress(string text) {
            BWTTransformer bwt = new BWTTransformer();

            var transformed = bwt.Transform(text);

            RLECompressor rle = new RLECompressor();

            var compressed = rle.Compress(transformed);

            return compressed;
        }
        public string Decompress(string text) {
            RLECompressor rle = new RLECompressor();

            var decompressed = rle.Decompress(text);

            BWTTransformer bwt = new BWTTransformer();

            var detransformed = bwt.BackwardTransform(decompressed);

            return detransformed;
        }
    }
}