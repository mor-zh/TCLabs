﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAlgorithms.LZ77
{
    internal class LZ77Buffer
    {
        private int _size;
        private StringBuilder _buffer;

        public LZ77Buffer(int size = 1024)
        {
            _size = size;
            _buffer = new StringBuilder(size);
        }

        public void AddString(StringBuilder @string)
        {
            if (_buffer.Length+@string.Length>_size)
                _buffer.Remove(0, @string.Length);
            _buffer.Append(@string);
        }

        public IEnumerable<int> GetAllShiftsOfSubstring(char @char)
        {
            var set = new HashSet<int>();

            if (@char == '\0')
                return set;

            for (var i = 0; i < _buffer.Length; i++)
                if (_buffer[i] == @char)
                    set.Add(_buffer.Length- i);

            return set;
        }

        public IEnumerable<int> GetAllShiftsOfSubstring(IEnumerable<int> searchAreaShifts, char @char)
        {
            var set = new HashSet<int>();

            if (@char == '\0')
                return set;

            foreach (var shift in searchAreaShifts)
            {
                var index = _buffer.Length - shift;
                if (index < _buffer.Length - 1 && _buffer[index + 1] == @char)
                    set.Add(shift - 1);
            }

            return set;
        }
    }
}
