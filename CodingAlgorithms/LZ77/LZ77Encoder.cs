﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodingAlgorithms.LZ77
{
    public class LZ77Encoder
    {
        public List<(int, int, char)> Alphabet = new List<(int, int, char)>();
        public string Encode(string source)
        {
            SetAlphabet(source);
            return string.Join(",", Alphabet.Select(x=>string.Format($"{x.Item1},{x.Item2},{(int)x.Item3}")));
        }

        public string Decode(string source)
        {
            var array = source.Split(',');
            var sb = new StringBuilder("");

            for (var i = 0; i < array.Length; i += 3)
            {
                var (shift, length,nextChar) = (int.Parse(array[i]), int.Parse(array[i + 1]), (char)int.Parse(array[i + 2]));
                var substring = new StringBuilder("");

                for (var j = sb.Length-shift;length>0 && j<sb.Length;j++)
                {
                    substring.Append(sb[j]);
                    length--;
                }

                substring.Append(nextChar);
                sb.Append(substring);
            }

            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        public void SetAlphabet(string source)
        {
            source += '\0';
            var buffer = new LZ77Buffer();
            var sb = new StringBuilder();
            var position = 1;
            var alphabet = new List<(int, int, char)>();
            IEnumerable<int> set = new HashSet<int>();

            for (var i = 0; i < source.Length; i++)
            {
                var @char = source[i];
                sb.Append(@char);

                if (sb.Length == 1)
                    set = buffer.GetAllShiftsOfSubstring(@char);
                else
                    set = buffer.GetAllShiftsOfSubstring(set, @char);

                position = set.Count() > 0 ? set.First() : position - 1;

                if (set.Count() > 0 && i + 1 < source.Length)
                    continue;

                var size = sb.Length - 1;
                alphabet.Add((position + size, size, sb[size]));
                position = 1;
                buffer.AddString(sb);
                sb.Clear();
            }

            Alphabet = alphabet;
        }

        public IEnumerable<string> GetAlphabetToString()
        {
            return Alphabet.Select(x => String.Format($"({x.Item1},{x.Item2},{(x.Item3 != '\0' ? x.Item3.ToString() : "eof")})"));
        }
    }
}
