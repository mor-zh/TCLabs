
namespace CodingAlgorithms.Huffman {
    public class Node : IComparable<Node> {
        public char? Symbol { get; set; } = null;
        public int Weight { get; set; }
        public string Code { get; set; } = "";
        public Node? Left {get; set;} = null;
        public Node? Right {get; set;} = null;

        public int CompareTo(Node other)
        {
            if (other is null) throw new ArgumentNullException();

            return Weight.CompareTo(other.Weight);
        }
    }
}