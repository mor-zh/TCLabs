
using System.Text;

namespace CodingAlgorithms.Huffman
{
    public class HuffmanEncoder
    {
        Node tree;

        public string Encode(string text)
        {
            var quants = GetCharsQuantites(text);
            var sortedQuants = SortCharQuantities(quants);
            tree = BuildTree(sortedQuants);
            var codes = BuildCodes(tree, "");
            var codesDict = AssortCodes(codes);
            var encodedMessage =  EncodeText(codesDict, text);

            System.Console.WriteLine($"Original message = {text.Length} bytes = {text.Length * 8} bits");
            System.Console.WriteLine($"Encoded message = {encodedMessage.Length} bits");
            System.Console.WriteLine($"Compression ratio = {encodedMessage.Length / (text.Length * 8)}");

            return encodedMessage;
        }
        public string Decode(string text)
        {
            StringBuilder sb = new StringBuilder();
            Node currentNode = tree;

            foreach (var symbol in text)
            {
                if (symbol == '0') {
                    currentNode = currentNode.Left;
                }
                else if (symbol == '1') {
                    currentNode = currentNode.Right;
                }

                if (currentNode.Symbol is not null) {
                    sb.Append(currentNode.Symbol);
                    currentNode = tree;
                }
            }

            return sb.ToString();
        }

        public string Decode(string text, Node tree) {
            this.tree = tree;

            return Decode(text);
        }

        public string Decode(string text, Dictionary<char, string> codes) {
            StringBuilder sb = new StringBuilder();
            StringBuilder decoded = new StringBuilder();
            
            foreach (var symbol in text)
            {
                sb.Append(symbol);

                if (codes.ContainsValue(sb.ToString())) {
                    decoded.Append(codes.First(p => p.Value == sb.ToString()).Key);
                    sb.Clear();
                }
            }

            return decoded.ToString();
        }

        public Dictionary<char, int> GetCharsQuantites(string text)
        {
            var charQuantity = new Dictionary<char, int>();

            foreach (var character in text)
            {
                if (charQuantity.ContainsKey(character))
                {
                    charQuantity[character]++;
                }
                else
                {
                    charQuantity.Add(character, 1);
                }
            }

            return charQuantity;
        }

        public List<Node> SortCharQuantities(Dictionary<char, int> quantities)
        {

            var fList = (from p in quantities
                         select new Node
                         {
                             Symbol = p.Key,
                             Weight = p.Value
                         })
                        .OrderBy(e => e.Weight)
                        .ToList<Node>();

            return fList;
        }

        public Node BuildTree(List<Node> nodes)
        {
            while (nodes.Count != 1)
            {
                Node combine = new Node();

                nodes[0].Code = "0";
                nodes[1].Code = "1";

                combine.Left = nodes[0];
                combine.Right = nodes[1];

                combine.Weight = nodes[0].Weight + nodes[1].Weight;

                nodes.RemoveRange(0, 2);
                nodes.Add(combine);
                nodes.Sort();
            }

            return nodes[0];
        }

        public List<KeyValuePair<char, string>> BuildCodes(Node node, string code)
        {
            List<KeyValuePair<char, string>> codes = new List<KeyValuePair<char, string>>();

            if (node.Symbol is not null)
            {
                codes.Add(new KeyValuePair<char, string>((char)node.Symbol, code));
                return codes;
            }

            if (node.Left is not null) 
            {
                codes.AddRange(BuildCodes(node.Left, code + node.Left.Code));
            }
            if (node.Right is not null) {
                codes.AddRange(BuildCodes(node.Right, code + node.Right.Code));
            }

            return codes;
        }

        public Dictionary<char,string> AssortCodes(List<KeyValuePair<char, string>> codes) {
            var sCodes = new Dictionary<char, string>();

            for (int i = 0; i < codes.Count; i++)
            {
                sCodes.Add(codes[i].Key, codes[i].Value);
            }

            return sCodes;
        }

        public string EncodeText(Dictionary<char,string> codes, string text) {
            StringBuilder sb = new StringBuilder();

            foreach (var sym in text)
            {
                sb.Append(codes[sym]);
            }

            return sb.ToString();
        }
    }
}