

namespace CodingAlgorithms.ShannonFano {
    public class ShannonFanoEntity : IComparable<ShannonFanoEntity> {
        public char Character { get; set; }
        public double Frequency { get; set; }
        public string Code { get; set; }

        public int CompareTo(ShannonFanoEntity other)
        {
            if (other is null) throw new ArgumentNullException();

            return Frequency.CompareTo(other.Frequency);
        }
    }
}