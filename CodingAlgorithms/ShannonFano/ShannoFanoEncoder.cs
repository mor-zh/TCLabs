using System.Text;

namespace CodingAlgorithms.ShannonFano
{
    public class ShannonFanoEncoder
    {
        Dictionary<char, string> _symbolCodes;
        public ShannonFanoEncoder()
        {
        }

        public string Encode(string text)
        {
            var quantities = GetCharsFrequencies(text);
            var qList = SortCharQuantities(quantities);
            var codes = BuildCodes(qList, "");
            _symbolCodes = AssortCodes(qList, codes);
            var encodedMessage = EncodeMessage(_symbolCodes, text);

            System.Console.WriteLine($"Original message = {text.Length} bytes = {text.Length * 8} bits");
            System.Console.WriteLine($"Encoded message = {encodedMessage.Length} bits");
            System.Console.WriteLine($"Compression ratio = {encodedMessage.Length / (text.Length * 8)}");

            return encodedMessage;
        }
        public string Decode(string text)
        {
            StringBuilder res = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                sb.Append(text[i]);

                try {
                    var p = _symbolCodes.First(p => p.Value == sb.ToString());

                    res.Append(p.Key);

                    sb.Clear();
                }
                catch {
                }
            }

            return res.ToString();;
        }
        public string Decode(string text, Dictionary<char, string> symbolCodes)
        {
            _symbolCodes = symbolCodes;

            return Decode(text);
        }
        public string Decode(string text, Dictionary<char, double> symbolFrequencies)
        {
            var qList = SortCharQuantities(symbolFrequencies);
            var codes = BuildCodes(qList, "");
            _symbolCodes = AssortCodes(qList, codes);
            var decodedMessage = Decode(text);

            return decodedMessage;
        }

        //Считаем частоты всех символов
        public Dictionary<char, double> GetCharsFrequencies(string text)
        {
            var charQuantity = new Dictionary<char, double>();

            foreach (var character in text)
            {
                if (charQuantity.ContainsKey(character))
                {
                    charQuantity[character]++;
                }
                else
                {
                    charQuantity.Add(character, 1);
                }
            }

            foreach (var ch in charQuantity)
            {
                charQuantity[ch.Key] /= text.Length;
            }

            return charQuantity;
        }

        //Сортируем частоты символов по возрастанию
        public List<ShannonFanoEntity> SortCharQuantities(Dictionary<char, double> quantities)
        {

            var fList = (from p in quantities select new ShannonFanoEntity { Character = p.Key, Frequency = p.Value })
                        .OrderByDescending(e => e.Frequency)
                        .ToList<ShannonFanoEntity>();

            return fList;
        }

        //Строим коды для каждого символа
        public List<string> BuildCodes(List<ShannonFanoEntity> list, string code)
        {
            List<string> codes = new List<string>();

            if (list.Count == 1)
            {
                codes.Add(code);
                return codes;
            }

            double sumLeft = 0;
            double sumRight = (from e in list select e.Frequency).Sum();
            double diff = Math.Abs(sumRight - sumLeft);

            int index = 0;

            //Делим исходный массив на 2, сумма частот символов в которых примерно одинакова
            for (int i = 0; i < list.Count; i++)
            {
                //Сумма частот символов в левом массиве
                var sumLeftTmp = sumLeft + list[i].Frequency;
                //Сумма частот в правом
                var sumRightTmp = sumRight - list[i].Frequency;

                var diffTmp = Math.Abs(sumRightTmp - sumLeftTmp);
                if (diffTmp < diff)
                {
                    diff = diffTmp;
                    index = i;
                    sumLeft = sumLeftTmp;
                    sumRight = sumRightTmp;
                }
                else
                {
                    break;
                }
            }

            //Рекурсивно строим коды для левого и правого массива
            //Левому массиву назначаем начальный символ 0    
            codes.AddRange(BuildCodes(list.GetRange(0, index + 1), code + "0"));
            //Правому массиву - начальный символ 1
            codes.AddRange(BuildCodes(list.GetRange(index + 1, list.Count - index - 1), code + "1"));

            return codes;
        }

        //Назначаем коды символам
        public Dictionary<char, string> AssortCodes(List<ShannonFanoEntity> characters, List<string> codes)
        {
            var _symbolCodes = new Dictionary<char, string>();
            for (int i = 0; i < codes.Count; i++)
            {
                _symbolCodes.Add(characters[i].Character, codes[i]);
            }
            return _symbolCodes;
        }

        //Кодируем сообщение
        public string EncodeMessage(Dictionary<char, string> codedSymbols, string text)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var symbol in text)
            {
                sb.Append(codedSymbols[symbol]);
            }

            return sb.ToString();
        }
    }
}